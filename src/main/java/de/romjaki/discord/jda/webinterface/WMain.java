package de.romjaki.discord.jda.webinterface;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpServer;
import com.sun.net.httpserver.spi.HttpServerProvider;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by RGR on 22.08.2017.
 */
public class WMain {

    public static void main(String[] args) {
        createWebsocket();
    }

    public static void createWebsocket() {
        try {
            HttpServer server = HttpServerProvider.provider().createHttpServer(new InetSocketAddress(8000), 0);
            server.createContext("/static").setHandler(new StaticHandler("src/main/resources"));
            server.createContext("/static/dashboard.html").setHandler(new DashboardHandler());
            server.createContext("/api/send/send").setHandler(new SendHandler());
            server.createContext("/api/send/channels").setHandler(new APISendEndpointHandler());
            server.createContext("/api/permissions/list").setHandler(new ListPermissionsHandler());
            server.createContext("/api/permissions/update").setHandler(new PermissionsUpdateHandler());
            server.createContext("/login").setHandler(new LoginHandler());
            server.createContext("/").setHandler(httpExchange -> {
                Headers responseHeaders = httpExchange.getResponseHeaders();
                responseHeaders.add("Location", "https://discordapp.com/oauth2/authorize?client_id=312256871174111233&response_type=code&scope=identify&redirect_uri=http://localhost:8000/login");
                httpExchange.sendResponseHeaders(302, -1);
            });
            server.start();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public static Map<String, String> parseQueryString(String qs) {
        Map<String, String> result = new HashMap<>();
        if (qs == null)
            return result;

        int last = 0, next, l = qs.length();
        while (last < l) {
            next = qs.indexOf('&', last);
            if (next == -1)
                next = l;

            if (next > last) {
                int eqPos = qs.indexOf('=', last);
                try {
                    if (eqPos < 0 || eqPos > next)
                        result.put(URLDecoder.decode(qs.substring(last, next), "utf-8"), "");
                    else
                        result.put(URLDecoder.decode(qs.substring(last, eqPos), "utf-8"), URLDecoder.decode(qs.substring(eqPos + 1, next), "utf-8"));
                } catch (UnsupportedEncodingException e) {
                    throw new RuntimeException(e); // will never happen, utf-8 support is mandatory for java
                }
            }
            last = next + 1;
        }
        return result;
    }
}
