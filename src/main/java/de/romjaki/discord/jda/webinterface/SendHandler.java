package de.romjaki.discord.jda.webinterface;

import de.romjaki.discord.jda.Permissions;
import net.dv8tion.jda.core.entities.TextChannel;

import java.util.Map;

import static de.romjaki.discord.jda.Main.jda;

public class SendHandler extends APIEndpoint {

    static {
        Permissions.addFlag("echo", 4);
    }

    @Override
    protected int requiredPermissions() {
        return Permissions.getAsFlag("echo");
    }

    protected String getResponse(Map<String, String> queryParams) {
        if (!queryParams.containsKey("id") || !queryParams.containsKey("text") || queryParams.get("text").trim().isEmpty()) {
            return "{\"success\":false,\"message\":\"Missing id or text\"}";
        }
        TextChannel channel = jda.getTextChannelById(queryParams.get("id"));
        if (channel == null || !channel.canTalk()) {
            return "{\"success\":false,\"message\":\"Invalid Channel id\"}";
        }
        try {
            channel.sendMessage(queryParams.get("text")).queue();
        } catch (Exception e) {
            return "{\"success\":false,\"message\":\"" + e.getMessage().replace("\"", "\\\"") + "\"}";
        }
        return "{\"success\":true}";
    }
}
