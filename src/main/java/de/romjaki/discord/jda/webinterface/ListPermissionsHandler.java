package de.romjaki.discord.jda.webinterface;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import de.romjaki.discord.jda.Permissions;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.OutputStream;

public class ListPermissionsHandler implements HttpHandler {
    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        JSONArray array = new JSONArray();
        Permissions.permissionMap.forEach((user, integer) -> {
            JSONObject object = new JSONObject();
            JSONObject userObj = new JSONObject();
            userObj.put("name", user.getName());
            userObj.put("id", user.getId());
            userObj.put("icon", user.getEffectiveAvatarUrl());
            object.put("user", userObj);
            object.put("permission", integer.intValue());
            array.put(object);
        });
        byte[] resp = array.toString().getBytes();
        httpExchange.sendResponseHeaders(200, resp.length);
        OutputStream responseBody = httpExchange.getResponseBody();
        responseBody.write(resp);
        responseBody.close();
    }
}
