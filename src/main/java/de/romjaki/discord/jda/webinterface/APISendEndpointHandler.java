package de.romjaki.discord.jda.webinterface;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import de.romjaki.discord.jda.Main;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.OutputStream;

public class APISendEndpointHandler implements HttpHandler {
    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        JSONArray array = new JSONArray();
        Main.jda.getGuilds().forEach(guild -> {
            JSONObject o = new JSONObject();
            JSONArray channels = new JSONArray();
            guild.getTextChannels().forEach(textChannel -> {
                if (textChannel.canTalk()) {
                    JSONObject channel = new JSONObject();
                    channel.put("name", textChannel.getName());
                    channel.put("id", textChannel.getId());
                    channels.put(channel);
                }
            });
            o.put("channels", channels);
            o.put("name", guild.getName());
            o.put("icon", guild.getIconUrl());
            array.put(o);
        });
        byte[] mes = array.toString().getBytes();
        httpExchange.sendResponseHeaders(200, mes.length);
        OutputStream responseBody = httpExchange.getResponseBody();
        responseBody.write(mes);
        responseBody.close();
    }
}
