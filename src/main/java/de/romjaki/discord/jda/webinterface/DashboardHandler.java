package de.romjaki.discord.jda.webinterface;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;

import static de.romjaki.discord.jda.webinterface.WMain.parseQueryString;

public class DashboardHandler implements HttpHandler {
    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        Map<String, String> queryParams = parseQueryString(httpExchange.getRequestURI().getQuery());

        if (!queryParams.containsKey("token")) {
            httpExchange.getResponseHeaders().add("Location", "/");
            httpExchange.sendResponseHeaders(302, -1);
            return;
        }

        byte[] text = FileUtils.readFileToString(new File("src/main/resources/dashboard.html"), "UTF-8").replace("{TOKEN}", queryParams.get("token")).getBytes();
        httpExchange.sendResponseHeaders(200, text.length);
        OutputStream responseBody = httpExchange.getResponseBody();
        responseBody.write(text);
        responseBody.close();
    }
}
