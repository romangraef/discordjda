package de.romjaki.discord.jda.webinterface;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import de.romjaki.discord.jda.Permissions;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URISyntaxException;
import java.util.Map;

import static de.romjaki.discord.jda.Main.jda;
import static de.romjaki.discord.jda.webinterface.WMain.parseQueryString;

public abstract class APIEndpoint implements HttpHandler {
    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        Map<String, String> queryParams = parseQueryString(httpExchange.getRequestURI().getQuery());
        if (!queryParams.containsKey("token")) {
            httpExchange.getResponseHeaders().add("Location", "/");
            httpExchange.sendResponseHeaders(302, -1);
            return;
        }

        HttpClient client = HttpClientBuilder.create().build();
        URIBuilder builder = new URIBuilder();
        builder.setScheme("https")
                .setHost("discordapp.com")
                .setPath("api/v6/users/@me");
        HttpGet get;
        try {
            get = new HttpGet(builder.build());
        } catch (URISyntaxException e) {
            httpExchange.sendResponseHeaders(500, -1);
            return;
        }
        get.addHeader("Authorisation", "Bearer " + queryParams.get("token"));
        HttpResponse execute = client.execute(get);
        JSONObject o = new JSONObject(EntityUtils.toString(execute.getEntity()));
        String id = o.getString("id");
        int perms = Permissions.getPermissions(jda.getUserById(id));
        byte[] resp;
        if ((perms & requiredPermissions()) != requiredPermissions()) {
            resp = "{\"success\":false,\"message\":\"Lacking permissions\"}".getBytes();
        } else {
            resp = getResponse(queryParams).getBytes();
        }
        httpExchange.sendResponseHeaders(200, resp.length);
        OutputStream responseBody = httpExchange.getResponseBody();
        responseBody.write(resp);
        responseBody.close();
    }

    protected abstract int requiredPermissions();

    protected abstract String getResponse(Map<String, String> queryParams);
}
