package de.romjaki.discord.jda.webinterface;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import de.romjaki.discord.jda.Constants;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Map;

import static de.romjaki.discord.jda.webinterface.WMain.parseQueryString;

public class LoginHandler implements HttpHandler {
    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        Map<String, String> queryParams = parseQueryString(httpExchange.getRequestURI().getQuery());
        if (!queryParams.containsKey("code")) {
            httpExchange.getResponseHeaders().add("Location", "/");
            httpExchange.sendResponseHeaders(302, -1);
        }
        HttpClient client = HttpClientBuilder.create().build();
        URIBuilder builder = new URIBuilder();
        builder.setScheme("https")
                .setHost("discordapp.com")
                .setPath("api/v6/oauth2/token")
                .addParameter("client_id", Constants.DiscordUser.CLIENT_ID)
                .addParameter("client_secret", Constants.DiscordUser.CLIENT_SECRET)
                .addParameter("code", queryParams.get("code"))
                .addParameter("grant_type", "authorization_code")
                .addParameter("redirect_uri", "http://localhost:8000/login");
        HttpPost post = null;
        try {
            post = new HttpPost(builder.build());
        } catch (URISyntaxException e) {
            httpExchange.sendResponseHeaders(500, -1);
            return;
        }
        post.addHeader("Content-Type", "application/x-www-form-urlencoded");
        HttpResponse execute = client.execute(post);
        String json = EntityUtils.toString(execute.getEntity());
        JSONObject object = new JSONObject(json);
        String token = object.getString("access_token");
        httpExchange.getResponseHeaders().add("Location", "/static/dashboard.html?token=" + token);
        httpExchange.sendResponseHeaders(302, -1);

    }
}
