package de.romjaki.discord.jda.webinterface;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;

public class StaticHandler implements HttpHandler {
    private String dir;


    public StaticHandler(String dir) {
        this.dir = dir;
    }

    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        URI uri = httpExchange.getRequestURI();
        String path = uri.getPath().replaceFirst("/static", "");
        File file = new File(dir + path);
        if (!file.exists()) {
            String res = "404 Not found";
            httpExchange.sendResponseHeaders(404, res.getBytes().length);
            OutputStream os = httpExchange.getResponseBody();
            os.write(res.getBytes());
            os.close();
            return;
        }

        String mime = "text/plain";
        String fileName = file.getName();
        if (fileName.endsWith(".js")) {
            mime = "application/javascript";
        } else if (fileName.endsWith(".css")) {
            mime = "text/css";
        } else if (fileName.endsWith(".html")) {
            mime = "text/html";
        }
        Headers h = httpExchange.getResponseHeaders();
        h.add("Content-Type", mime);
        httpExchange.sendResponseHeaders(200, 0);
        FileInputStream fis = new FileInputStream(file);
        OutputStream rb = httpExchange.getResponseBody();
        IOUtils.copy(fis, rb);
        rb.close();
    }
}
