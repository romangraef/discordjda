package de.romjaki.discord.jda.webinterface;

import de.romjaki.discord.jda.Permissions;
import net.dv8tion.jda.core.entities.User;

import java.util.Map;

import static de.romjaki.discord.jda.Main.jda;

public class PermissionsUpdateHandler extends APIEndpoint {

    @Override
    protected int requiredPermissions() {
        return -1;
    }

    protected String getResponse(Map<String, String> queryParams) {
        if (!queryParams.containsKey("id") || !queryParams.containsKey("new_permissions")) {
            return "{\"success\":false,\"message\":\"Missing id or permissions\"}";
        }
        User u = jda.getUserById(queryParams.get("id"));
        if (u == null) {
            return "{\"success\":false,\"message\":\"Invalid user id\"}";
        }
        if (!queryParams.get("new_permissions").matches("^[0-9]+$")) {
            return "{\"success\":false,\"message\":\"Invalid permissions\"}";
        }
        try {
            Permissions.setPermissions(u, Integer.parseInt(queryParams.get("new_permissions")));
        } catch (Exception e) {
            return "{\"success\":false,\"message\":\"" + e.getMessage() + "\"}";
        }
        return "{\"success\":true}";
    }
}
