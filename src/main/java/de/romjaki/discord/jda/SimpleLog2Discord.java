package de.romjaki.discord.jda;

import com.google.common.base.Splitter;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.utils.SimpleLog;
import org.slf4j.event.Level;

import static de.romjaki.discord.jda.Main.jda;

/**
 * Created by RGR on 21.06.2017.
 */
public class SimpleLog2Discord {
    public static void addLogChannel(TextChannel channel) {
        SimpleLog.addListener(new SimpleLog.LogListener() {


            @Override
            public void onLog(SimpleLog log, Level logLevel, Object message) {
                String name = log.getName();
                if (name.contains("unlogged")) {
                    return;
                }
                String content = message + "";
                if (logLevel == Level.TRACE || logLevel == Level.DEBUG) return;
                if (!channel.canTalk()) return;
                for (String s : Splitter.fixedLength(2000).omitEmptyStrings().split("[" + logLevel.toString() + "]" + content)) {
                    channel.sendMessage(s).queue();
                }
            }

            @Override
            public void onError(SimpleLog log, Throwable err) {
                onLog(log, Level.ERROR, "<@!310702108997320705>" + err);
            }
        });
    }
}
