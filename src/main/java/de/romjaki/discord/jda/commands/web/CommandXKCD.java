package de.romjaki.discord.jda.commands.web;

import de.romjaki.discord.jda.Cache;
import de.romjaki.discord.jda.Command;
import de.romjaki.discord.jda.Commands;
import de.romjaki.discord.jda.UnUtil;
import de.romjaki.discord.jda.commands.Category;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.*;
import net.dv8tion.jda.core.events.message.react.MessageReactionAddEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;
import net.dv8tion.jda.core.utils.SimpleLog;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static de.romjaki.discord.jda.Constants.gson;
import static de.romjaki.discord.jda.Main.jda;
import static de.romjaki.discord.jda.UnUtil.RandomUtils.randomColor;
import static java.util.concurrent.TimeUnit.SECONDS;

public class CommandXKCD extends ListenerAdapter implements Command {
    private static final String XKCD_FOOTER_ICON = "https://cdn3.dontpayfull.com/media/logos/m/xkcd.com.jpg";
    private static final String XKCD_FOOTER = "%d | XKCD Comic";
    private static final Pattern FOOTER_PATTERN = Pattern.compile("^(?<id>[0-9]+) \\| XKCD Comic$");
    private static final String ANY_API_URL = "https://xkcd.com/%d/info.0.json";
    private static final String LEFT_EMOTE = "\u2b05";
    private static final String CLOSE_EMOTE = "\u274c";
    private static final String RIGHT_EMOTE = "\u27a1";
    private static final String LATEST_API_URL = "https://xkcd.com/info.0.json";
    private static final XKCDComic COMIC_404 = new XKCDComic() {{
        link = "https://xkcd.com/404";

        day = "6";
        month = "6";
        year = "6";

        num = 404;

        title = "Not found";
        safe_title = title;
        alt = "There is no comic. Sorry";
        news = "";
        img = "https://logl.com/logloglgogoglgolgol.png.jpg.wav.jpg";

        transcript = "404 Transcript not found";
    }};
    private static Cache<Integer> latestComicId = new Cache<>(() -> {
        try {
            return gson.fromJson(UnUtil.getWebContent(new URL(LATEST_API_URL)), XKCDComic.class).num;
        } catch (MalformedURLException e) {
            SimpleLog.getLog("web").fatal(e);
        }
        return null;
    }, 1000 * 60 * 60 * 24);

    public static XKCDComic fetchComic(int id) {
        if (id < 1 || id > latestComicId.get()) {
            return null;
        }
        if (id == 404) {
            return COMIC_404;
        }
        try {
            return gson.fromJson(UnUtil.getWebContent(new URL(String.format(ANY_API_URL, id))), XKCDComic.class);
        } catch (MalformedURLException e) {
            SimpleLog.getLog("web").fatal(e);
            return null;
        }
    }

    @Override
    public String getName() {
        return "xkcd";
    }

    @Override
    public void execute(String[] args, Guild guild, TextChannel channel, Member member, Message message) {
        if (args.length > 2) {
            displaySyntaxError(channel);
            return;
        }
        int toDisplay = latestComicId.get();
        if (args.length == 1) {
            try {
                toDisplay = Integer.parseInt(args[0]);
                if (toDisplay < 1 || toDisplay > latestComicId.get()) {
                    displaySyntaxError(channel);
                    return;
                }
            } catch (NumberFormatException e) {
                displaySyntaxError(channel);
                return;
            }
        }
        Message mes = channel.sendMessage(displayComic(toDisplay)).complete();
        mes.addReaction(LEFT_EMOTE).complete();
        mes.addReaction(CLOSE_EMOTE).complete();
        mes.addReaction(RIGHT_EMOTE).complete();
    }

    private MessageEmbed displayComic(int toDisplay) {
        XKCDComic comic = fetchComic(toDisplay);
        return new EmbedBuilder()
                .setImage(comic.img)
                .setTitle(comic.title, comic.link)
                .setDescription(comic.alt)
                .setColor(randomColor())
                .setFooter(String.format(XKCD_FOOTER, toDisplay), XKCD_FOOTER_ICON)
                .build();
    }

    @Override
    public void onMessageReactionAdd(MessageReactionAddEvent event) {
        super.onMessageReactionAdd(event);
        if (event.getUser().equals(jda.getSelfUser())) {
            return;
        }
        Message message = event.getTextChannel().getMessageById(event.getMessageId()).complete();
        if (!message.getAuthor().equals(jda.getSelfUser())) {
            return;
        }
        if (message.getEmbeds().size() != 1) {
            return;
        }
        MessageEmbed embed = message.getEmbeds().get(0);
        String footer = embed.getFooter().getText();
        Matcher matcher = FOOTER_PATTERN.matcher(footer);
        if (!matcher.matches()) {
            return;
        }
        event.getReaction().removeReaction(event.getUser()).queue();
        int id = Integer.parseInt(matcher.group("id"));
        String emoteText = event.getReactionEmote().toString();
        if (emoteText.contains(CLOSE_EMOTE)) {
            message.delete().queue();
            return;
        }
        if (emoteText.contains(RIGHT_EMOTE)) {
            id++;
        }
        if (emoteText.contains(LEFT_EMOTE)) {
            id--;
        }
        if (id < 1 || id > latestComicId.get()) {
            return;
        }
        message.editMessage(displayComic(id)).queue();
    }

    public void displaySyntaxError(TextChannel channel) {
        channel.sendMessage(new EmbedBuilder()
                .setTitle("Syntax error")
                .setDescription(getSyntax())
                .setColor(randomColor())
                .build()).queue(msg -> msg.delete().queueAfter(5, SECONDS));
    }

    /*
     *
     *
     * BOT STUFF
     *
     *
     *
     */


    @Override
    public boolean requiresBotChannel() {
        return true;
    }

    @Override
    public String getDescription() {
        return "Shows you todays or any xkcd comic";
    }

    @Override
    public String getSyntax() {
        return "[id]";
    }

    @Override
    public String getTopicRequirement() {
        return "allowXKCD";
    }

    @Override
    public int getRequiredClientPermission() {
        return 0;
    }

    @Override
    public Permission[] getRequiredServerPermission() {
        return new Permission[0];
    }

    @Override
    public Category getCategory() {
        return Commands.getCategory("web");
    }


    @Override
    public Command register(JDA jda) {
        jda.addEventListener(this);
        return this;
    }
}
