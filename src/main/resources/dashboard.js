let req;

function sidebar_close() {
    $("#sidebar").fadeOut();
}

function sidebar_open() {
    $("#sidebar").fadeIn();
}

function load_waiting() {
    $("#content").html("<div class='spinner centered'></div>");
}

function guild_icon_for(guild) {
    return guild.icon ? guild.icon : 'https://media-elerium.cursecdn.com/avatars/33/19/635889567578916779.png'
}


function parse_send_data(data) {
    data = JSON.parse(data);
    let content = "";
    data.forEach(function (guild) {
        content += `<div><img width="32" height="32" class="img-circle" src="${guild_icon_for(guild)}"/>${guild.name}`;
        guild.channels.forEach(function (channel) {
            content += `<p class="send-channel"><a href="#" onclick="load_send_message_dialog('${channel.id}','${channel.name}','${guild.icon}')">#${channel.name}</a></p>`;
        });
        content += "</div>";
    });
    $("#content").html(content);
}

function send_message_success(data) {
    data = JSON.parse(data);
    console.log(data);
    $('#content').html(data.success ?
        'Successfully sent message' :
        `<div class="error">
${data.message}
</div>`);
}

function send_message_to(id) {
    let text = $('#toSend').val();
    clear_requests();
    load_waiting();
    req = $.get('/api/send/send', {id: id, text: text, token: TOKEN}, send_message_success);
}

function load_send_message_dialog(id, name, icon) {
    $("#content").html(`
<h4>Sending to <img width="16" height="16" src="${guild_icon_for({icon: icon})}">#${name}</h4>
<div class="group">
    <input type="text" id="toSend" required>
    <span class="highlight"></span>
    <span class="bar"></span>
    <label>Text</label>
</div>
<a href="#" class="button" onclick="send_message_to('${id}')">Send</a>
`);
}

function clear_requests() {
    if (req) {
        req.abort();
        req = null;
    }
}

function load_send_menu() {
    clear_requests();
    load_waiting();
    sidebar_close();
    $("#header").html("Send messages");
    req = $.get('/api/send/channels', {token: TOKEN}, parse_send_data);
}

function edit_finished(data) {
    data = JSON.parse(data);
    $('#content').html(data.success ?
        `<a href="#" onclick="load_permissions_menu()">&lArr;Back</a>` :
        data.message);
}

function send_edit_permission(id) {
    let newPerm = parseInt($('#permission_bits').val(), 2).toString(10);
    load_waiting();
    clear_requests();
    req = $.get('/api/permissions/update', {new_permissions: newPerm, id: id, token: TOKEN}, edit_finished);
}

function edit_permissions(id, permission, name, icon) {
    $('#content').html(`
    <img style="float:left" src="${icon}"><div style="float:right"><ul>
    <li>Name: ${name}</li>
    <li>Id: ${id}</li>
    <li>Permission(base 10): ${permission}</li>
    <li>Permission(base 16): ${parseInt(permission).toString(16)}</li>
    <li>Permission:</li>
</ul>
<div class="group">
    <input type="text" id="permission_bits" required value="${parseInt(permission).toString(2)}">
    <span class="highlight"></span>
    <span class="bar"></span>
    <label>Bits</label>
</div>
<a href="#" onclick="send_edit_permission('${id}')" class="button"></a>
</div>
    `);
}

function parse_permissions_data(data) {
    data = JSON.parse(data);
    let content = "";
    data.forEach(function (item) {
        const user = item.user;
        const permission = item.permission;
        const name = user.name;
        const icon = user.icon;
        const id = user.id;
        content += `<a href="#" onclick="edit_permissions('${id}','${permission}','${name}','${icon}')" class="button">Edit permissions for <img width="16" height="16" src="${icon}">@${name}</a>`;
    });
    $('#content').html(content)
}

function load_permissions_menu() {
    clear_requests();
    load_waiting();
    sidebar_close();
    $("#header").html("Permissions");
    req = $.get('/api/permissions/list', {token: TOKEN}, parse_permissions_data);
}

